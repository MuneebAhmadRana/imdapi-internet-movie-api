using AutoMapper;
using IMDapi.Models.Domain;
using IMDapi.Models.DTO.CharacterDTOs;
using IMDapi.Models.DTO.MovieDTOs;
using IMDapi.Models.DTO.FranchiseDTOs;
using System.Linq;

namespace IMDapi.Profiles
{
    public class IMDapiProfile : Profile
    {
        public IMDapiProfile()
        {
            // Character models
            // Mapping characteres movies from MovieDMs to just movie Ids
            CreateMap<CharacterDM, CharacterDTO>()
                                .ForMember(destination => destination.MoviesId,
                                           opt =>
                                                opt.MapFrom(
                                                    source => source.Movies.Select(m => m.MovieId).ToArray()
                                                    )
                                            ).ReverseMap();

            CreateMap<CharacterPostDTO, CharacterDM>();
            CreateMap<CharacterDM, CharacterWithoutMoviesDTO>();

            // Movie models
            CreateMap<MovieDM, MovieDTO>()
                                .ForMember(destination => destination.CharactersId,
                                           opt =>
                                                opt.MapFrom(
                                                    source => source.Characters.Select(m => m.CharacterId).ToArray()
                                                    )
                                            ).ReverseMap();
            CreateMap<MoviePostDTO, MovieDM>();
            CreateMap<MovieDM, MovieWithoutFranchiseDTO>();

            //Franchise Models
            CreateMap<FranchiseDM, FranchiseDTO>()
                                .ForMember(destination => destination.MoviesId,
                                           opt =>
                                                opt.MapFrom(
                                                    source => source.Movies.Select(m => m.MovieId).ToArray()
                                                    )
                                            ).ReverseMap();
        }
    }
}