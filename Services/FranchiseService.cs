using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IMDapi.Models.Domain;
using IMDapi.Models.DTO.CharacterDTOs;
using IMDapi.Models.DTO.MovieDTOs;
using IMDapi.Models.DTO.FranchiseDTOs;
using Microsoft.EntityFrameworkCore;

namespace IMDapi.Services
{
    /// <summary>
    /// Franchise class which handles all databse queries to the Franchise table
    /// </summary>
    public class FranchiseService
    {
        private readonly IMDapiDBContext _DbContext;
        private readonly IMapper _mapper;
        public FranchiseService(IMDapiDBContext context, IMapper mapper)
        {
            _DbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all existing franchises
        /// </summary>
        /// <returns>List of franchises</returns>
        public IEnumerable<FranchiseDTO> GetFranchises()
        {
            IEnumerable<FranchiseDM> franchisesDM = _DbContext.Franchises.Include(f => f.Movies).ToList();
            IEnumerable<FranchiseDTO> franchisesDTO = _mapper.Map<IEnumerable<FranchiseDTO>>(franchisesDM);

            return franchisesDTO;
        }

        /// <summary>
        /// Gets a Franchise by Id
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>A franchise</returns>
        public async Task<FranchiseDTO> GetFranchise(int id)
        {
            FranchiseDM franchiseDM = await _DbContext.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.FranchiseId == id);
            FranchiseDTO franchiseDTO = _mapper.Map<FranchiseDTO>(franchiseDM);


            return franchiseDTO;
        }

        /// <summary>
        /// Adds a new franchise to the database
        /// </summary>
        /// <param name="franchiseDTO">Franchise to be added</param>
        /// <returns>The created franchise</returns>
        public async Task<FranchiseDM> PostFranchise(FranchiseDTO franchiseDTO)
        {
            FranchiseDM franchiseDM = _mapper.Map<FranchiseDM>(franchiseDTO);
            await _DbContext.Franchises.AddAsync(franchiseDM);

            return await _DbContext.SaveChangesAsync() > 0 ? franchiseDM : null;
        }

        /// <summary>
        /// Updates an existing Franchise
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="franchiseDTO">Updated franchise info</param>
        /// <returns>true if succesfull, false otherwise</returns>
        public async Task<bool> UpdateFranchise(int id, FranchiseDTO franchiseDTO)
        {
            if (id != franchiseDTO.FranchiseId)
            {
                return false;
            }
            FranchiseDM franchiseDM = _mapper.Map<FranchiseDM>(franchiseDTO);

            _DbContext.Franchises.Update(franchiseDM);
            await _DbContext.SaveChangesAsync();
            return true;
        }


        /// <summary>
        /// Deletes a franchise from the database
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>true if succesfull, false otherwise</returns>
        public async Task<bool> DeleteFranchise(int id)
        {
            FranchiseDM franchise = await _DbContext.Franchises.FirstOrDefaultAsync(f => f.FranchiseId == id);
            if (franchise is not null)
            {
                _DbContext.Franchises.Remove(franchise);
                await _DbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }


        /// <summary>
        /// Gets all the characters from a franchise
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>List of characters</returns>
        public async Task<IEnumerable<CharacterWithoutMoviesDTO>> GetFranchiseCharacters(int id)
        {
            var characters = _DbContext.Movies.Where(m => m.FranchiseId == id).Select(m => m.Characters);
            if (!characters.Any())
            {
                return null;
            }

            var characterList = new List<CharacterDM>();
            foreach (IEnumerable<CharacterDM> a in await characters.ToListAsync())
            {
                foreach (CharacterDM c in a)
                {
                    if (!characterList.Contains(c))
                    {
                        characterList.Add(c);
                    }
                }
            }

            return _mapper.Map<IEnumerable<CharacterWithoutMoviesDTO>>(characterList);
        }

        /// <summary>
        /// Gets all the movies from a franchise
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>list of movies</returns>
        public async Task<IEnumerable<MovieWithoutFranchiseDTO>> GetFranchiseMovies(int id)
        {
            var movies = await _DbContext.Movies.Where(m => m.FranchiseId == id).ToListAsync();
            
            return _mapper.Map<IEnumerable<MovieWithoutFranchiseDTO>>(movies);
        }
    }
}