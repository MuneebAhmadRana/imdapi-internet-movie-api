using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IMDapi.Models.Domain;
using IMDapi.Models.DTO;
using IMDapi.Models.DTO.CharacterDTOs;
using Microsoft.EntityFrameworkCore;

namespace IMDapi.Services
{
    public class CharacterService
    {
        private readonly IMDapiDBContext _DbContext;
        private readonly IMapper _mapper;
        public CharacterService(IMDapiDBContext context, IMapper mapper)
        {
            _DbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all existing characters
        /// </summary>
        /// <returns>List of characters</returns>
        public IEnumerable<CharacterDTO> GetCharacters()
        {
            IEnumerable<CharacterDM> charactersDM = _DbContext.Characters.Include(c => c.Movies).ToList();
            IEnumerable<CharacterDTO> charactersDTO = _mapper.Map<IEnumerable<CharacterDTO>>(charactersDM);

            return charactersDTO;
        }

        /// <summary>
        /// Gets a Character by Id
        /// </summary>
        /// <param name="id">Id of the character</param>
        /// <returns>A character</returns>
        public async Task<CharacterDTO> GetCharacter(int id)
        {
            CharacterDM characterDM = await _DbContext.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.CharacterId == id);
            CharacterDTO characterDTO = _mapper.Map<CharacterDTO>(characterDM);

            return characterDTO;
        }

        /// <summary>
        /// Adds a new character to the database
        /// </summary>
        /// <param name="characterDTO">Character to be added</param>
        /// <returns>The created character</returns>
        public async Task<CharacterDM> PostCharacter(CharacterPostDTO characterDTO)
        {
            CharacterDM characterDM = _mapper.Map<CharacterDM>(characterDTO);
            if (characterDTO.MoviesId is not null)
            {
                //Creating a new list
                characterDM.Movies = new List<MovieDM>();
                foreach (int movieId in characterDTO.MoviesId)
                {
                    var movie = await _DbContext.Movies.FirstOrDefaultAsync(m => m.MovieId == movieId);
                    //Checking if movie with the specified Id exists 
                    if(movie is null)
                    {
                        return null;
                    }
                    _DbContext.Movies.Add(movie);
                }
            }
            await _DbContext.Characters.AddAsync(characterDM);

            return await _DbContext.SaveChangesAsync() > 0 ? characterDM : null;
        }

        /// <summary>
        /// Updates an existing character
        /// </summary>
        /// <param name="id">Id of the character</param>
        /// <param name="characterDTO">Updated character info</param>
        /// <returns>true if succesfull, false otherwise</returns>
        public async Task<bool> UpdateCharacter(int id, CharacterDTO characterDTO)
        {
            if (id != characterDTO.CharacterId)
            {
                return false;
            }
            CharacterDM characterDM = _mapper.Map<CharacterDM>(characterDTO);

            _DbContext.Characters.Update(characterDM);
            await _DbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Deletes a character from the database
        /// </summary>
        /// <param name="id">Id of the Character</param>
        /// <returns>true if succesfull, false otherwise</returns>
        public async Task<bool> DeleteCharacter(int id)
        {
            CharacterDM character = await _DbContext.Characters.FirstOrDefaultAsync(c => c.CharacterId == id);
            if (character is not null)
            {
                System.Console.WriteLine(character.FullName);
                _DbContext.Characters.Remove(character);
                await _DbContext.SaveChangesAsync();
                return true;
            }
            return false;
           
        }
    }
}
