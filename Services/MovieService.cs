using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IMDapi.Models.Domain;
using IMDapi.Models.DTO.CharacterDTOs;
using IMDapi.Models.DTO.MovieDTOs;
using Microsoft.EntityFrameworkCore;

namespace IMDapi.Services
{
    /// <summary>
    /// Service class which handles all databse queries to the Movie table
    /// </summary>
    public class MovieService
    {
        private readonly IMDapiDBContext _DbContext;
        private readonly IMapper _mapper;

        public MovieService(IMDapiDBContext context, IMapper mapper)
        {
            _DbContext = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all existing movies
        /// </summary>
        /// <returns>List of movies</returns>
        public IEnumerable<MovieDTO> GetMovies()
        {
            IEnumerable<MovieDM> moviesDM = _DbContext.Movies.Include(m => m.Characters).ToList();
            IEnumerable<MovieDTO> moviesDTO = _mapper.Map<IEnumerable<MovieDTO>>(moviesDM);

            return moviesDTO;
        }

        /// <summary>
        /// Gets a Movie by Id
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <returns>A movie</returns>
        public async Task<MovieDTO> GetMovie(int id)
        {
            MovieDM movieDM = await _DbContext.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.MovieId == id);
            if(movieDM is null)
            {
                return null;
            }
            MovieDTO movieDTO = _mapper.Map<MovieDTO>(movieDM);

            return movieDTO;
        }

        /// <summary>
        /// Adds a new movie to the database
        /// </summary>
        /// <param name="movieDTO">Movie to be added</param>
        /// <returns>The created movie</returns>
        public async Task<MovieDM> PostMovie(MoviePostDTO movieDTO)
        {
            MovieDM movieDM = _mapper.Map<MovieDM>(movieDTO);
            var franchise = await _DbContext.Franchises.FirstOrDefaultAsync(f => f.FranchiseId == movieDTO.FranchiseId);
            //If franchise specified in the movieDTO does not exist, will return null
            if(franchise is null)
            {
                return null;
            }
            //checking if there are characters inside the movieDto
            if (movieDTO.CharactersId is not null)
            {
                movieDM.Characters = new List<CharacterDM>();
                foreach (int id in movieDTO.CharactersId)
                {
                    // Adds character to the movie list if the character exists
                    var character = await _DbContext.Characters.FirstOrDefaultAsync(c => c.CharacterId == id);
                    if (character is null)
                    {
                        return null;
                    }
                    movieDM.Characters.Add(character);
                }
            }

            movieDM.Franchise = franchise;
            await _DbContext.Movies.AddAsync(movieDM);

            return await _DbContext.SaveChangesAsync() > 0 ? movieDM : null;
        }

        /// <summary>
        /// Updates an existing movie
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <param name="movieDTO">Updated franchise info</param>
        /// <returns>true if succesfull, false otherwise</returns>
        public async Task<bool> UpdateMovie(int id, MovieDTO movieDTO)
        {
            if (id != movieDTO.MovieId)
            {
                return false;
            }

            MovieDM movieDM = _mapper.Map<MovieDM>(movieDTO);
            _DbContext.Movies.Update(movieDM);
            await _DbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Deletes a movie from the database
        /// </summary>
        /// <param name="id">Id of the Movie</param>
        /// <returns>true if succesfull, false otherwise</returns>
        public async Task<bool> DeleteMovie(int id)
        {
            MovieDM movie = await _DbContext.Movies.FirstOrDefaultAsync(m => m.MovieId == id);
            if (movie is not null)
            {
                _DbContext.Movies.Remove(movie);
                await _DbContext.SaveChangesAsync();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets all the characters from a movie
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <returns>List of characters</returns>
        public IEnumerable<CharacterWithoutMoviesDTO> GetMovieCharacters(int id)
        {
            
            var characters = _DbContext.Movies.Where(m => m.MovieId == id).Select(m => m.Characters);
            if(!characters.Any())
            {
                return null;
            }
            return _mapper.Map<IEnumerable<CharacterWithoutMoviesDTO>>(characters.Single());
        }
    }
}