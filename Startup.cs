using IMDapi.Models.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.IO;
using System.Reflection;
using IMDapi.Services;
using IMDapi.Profiles;

namespace IMDapi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Config Automapper
            services.AddAutoMapper(typeof(Startup));
            // Add Services for db access
            services.AddScoped<CharacterService>();
            services.AddScoped<FranchiseService>();
            services.AddScoped<MovieService>();

            //Adding DBContext as a service (providing connection string that is present in appsettings.json)
            services.AddDbContext<IMDapiDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("IMDapiDatabase")));
            services.AddControllers();
            //Register swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "IMDapi",
                    Description = "A datastore and interface to store and manipulate movie characters, movies and franchises.",
                    Contact = new OpenApiContact
                    {
                        Name = "Muneeb Rana / Andreas Vu",
                        Email = "muneeb.iar@gmail.com / andreas.loyning.vu@gmail.com",
                    },
                    License = new OpenApiLicense
                    {
                        Name = "GNU GENERAL PUBLIC LICENSE",
                        Url = new Uri("https://gitlab.com/MuneebAhmadRana/imdapi-internet-movie-api/-/blob/master/LICENSE"),
                    }

                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
