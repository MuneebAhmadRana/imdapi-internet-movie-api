﻿using IMDapi.Models.DTO.CharacterDTOs;
using IMDapi.Models.Domain;
using IMDapi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IMDapi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private readonly CharacterService _characterService;

        public CharacterController(CharacterService characterService)
        {
            _characterService = characterService;
        }

        /// <summary>
        /// Fetches all characters from the database
        /// </summary>
        /// <returns>
        /// Array of Characters
        /// </returns>
        [HttpGet]
        public ActionResult<IEnumerable<CharacterDTO>> Get()
        {
            return Ok(_characterService.GetCharacters());
        }

        /// <summary>
        /// Fetches character of the specified id
        /// </summary>
        /// <param name="id">Id of character</param>
        /// <returns>
        /// <see cref="CharacterDTO" />
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> Get(int id)
        {
            var character = await _characterService.GetCharacter(id);
            if (character is not null)
            {
                return character;
            }

            return NotFound();
        }

        /// <summary>
        /// Adds a new character to the database
        /// </summary>
        /// <param name="character">Character to be added</param>
        /// <returns>true if successfull, false otherwise</returns>
        [HttpPost]
        public async Task<ActionResult<bool>> Post(CharacterPostDTO character)
        {
            CharacterDM c = await _characterService.PostCharacter(character);
            if (c is null)
            {
                return BadRequest();
            }

            return CreatedAtAction(nameof(Get), new { id = c.CharacterId }, true);
        }

        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="id">character ID</param>
        /// <param name="character">New character info to be updated with</param>
        /// <returns>true if successfull, false otherwise</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<bool>> Put(int id, CharacterDTO character)
        {
            bool success = await _characterService.UpdateCharacter(id, character);
            if (!success)
            {
                return BadRequest();
            }

            return success;
        }

        /// <summary>
        /// Deletes the character of the specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            bool success = await _characterService.DeleteCharacter(id);
            if (!success)
            {
                return BadRequest();
            }

            return success;
        }
    }
}
