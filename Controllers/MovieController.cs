﻿using IMDapi.Models.DTO.CharacterDTOs;
using IMDapi.Models.DTO.MovieDTOs;
using IMDapi.Models.Domain;
using IMDapi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IMDapi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        // GET: api/<MovieController>
        private readonly MovieService _movieService;

        public MovieController(MovieService movieService)
        {
            _movieService = movieService;
        }

        /// <summary>
        /// Fetches all movies from the database
        /// </summary>
        /// <returns>
        /// Array of Movies
        /// </returns>
        [HttpGet]
        public IEnumerable<MovieDTO> Get()
        {
            return _movieService.GetMovies();
        }

        /// <summary>
        /// Fetches movie of the specified id
        /// </summary>
        /// <param name="id">Id of movie</param>
        /// <returns>
        /// <see cref="MovieDTO" />
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> Get(int id)
        {
            MovieDTO movie =  await _movieService.GetMovie(id);
            if(movie is null)
            {
                return NotFound($"Movie with the id {id} was not found");
            }

            return movie;
        }

        /// <summary>
        /// Adds a new movie to the database. Can link existing characters to movie
        /// </summary>
        /// <param name="movie">Movie to be added</param>
        /// <returns>true if successfull, false otherwise</returns>
        [HttpPost]
        public async Task<ActionResult<bool>> Post(MoviePostDTO movie)
        {
            MovieDM m =  await _movieService.PostMovie(movie);
            if(m is null)
            {
                return BadRequest();
            }

            return CreatedAtAction(nameof(Get), new { id = m.MovieId }, true);
        }

        /// <summary>
        /// Updates a movie
        /// </summary>
        /// <param name="id">movie ID</param>
        /// <param name="movie">New movie info to be updated with</param>
        /// <returns>true if successfull, false otherwise</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<bool>> Put(int id, MovieDTO movie)
        {
            bool success =  await _movieService.UpdateMovie(id, movie);
            if (!success)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes movie of the specified Id. Characters are not deleted.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            bool success = await _movieService.DeleteMovie(id);
            if (!success)
            {
                return BadRequest();
            }

            return success;
        }


        /// <summary>
        /// Fetches all Characters present in a movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public ActionResult<IEnumerable<CharacterWithoutMoviesDTO>> GetMovieCharacters(int id)
        {
            IEnumerable<CharacterWithoutMoviesDTO> charactersList = _movieService.GetMovieCharacters(id);
            if(charactersList is null)
            {
                return NotFound("Either movie not found, or no characters in that movie");
            }

            return Ok(charactersList);
        }
    }
}
