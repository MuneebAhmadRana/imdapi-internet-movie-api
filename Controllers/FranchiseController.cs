﻿using IMDapi.Models.Domain;
using IMDapi.Models.DTO.CharacterDTOs;
using IMDapi.Models.DTO.MovieDTOs;
using IMDapi.Models.DTO.FranchiseDTOs;
using IMDapi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IMDapi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private readonly FranchiseService _franchiseService;

        public FranchiseController(FranchiseService franchiseService)
        {
            _franchiseService = franchiseService;
        }
        /// <summary>
        /// Fetches all franchises from the database
        /// </summary>
        /// <returns>
        /// Array of Franchises
        /// </returns>
        [HttpGet]
        public ActionResult<IEnumerable<FranchiseDTO>> Get()
        {
            return Ok(_franchiseService.GetFranchises());
        }

        /// <summary>
        /// Fetches franchise of the specified id
        /// </summary>
        /// <param name="id">Id of franchise</param>
        /// <returns>
        /// <see cref="FranchiseDTO" />
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> Get(int id)
        {
            FranchiseDTO f = await _franchiseService.GetFranchise(id);
            if (f is null)
            {
                return NotFound();
            }

            return f;
        }

        /// <summary>
        /// Adds a new franchise to the database
        /// </summary>
        /// <param name="franchise">Franchise to be added</param>
        /// <returns>true if successfull, false otherwise</returns>
        [HttpPost]
        public async Task<ActionResult<bool>> Post(FranchiseDTO franchise)
        {
            FranchiseDM f = await _franchiseService.PostFranchise(franchise);
            if(f is null)
            {
                return BadRequest();
            }

            return CreatedAtAction(nameof(Get), new { id = f.FranchiseId }, true);
        }

        /// <summary>
        /// Updates a franchise
        /// </summary>
        /// <param name="id">franchise ID</param>
        /// <param name="franchise">New franchise info to be updated with</param>
        /// <returns>true if successfull, false otherwise</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<bool>> Put(int id, FranchiseDTO franchise)
        {
            bool success = await _franchiseService.UpdateFranchise(id, franchise);
            if (!success)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a franchise of the specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True or false based on outcome</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            bool success = await _franchiseService.DeleteFranchise(id);
            if (!success)
            {
                return BadRequest();
            }

            return success;
        }

        /// <summary>
        /// Returns all characters in a Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Characters</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterWithoutMoviesDTO>>> CharactersInFranchise(int id)
        {
            IEnumerable<CharacterWithoutMoviesDTO> c =  await _franchiseService.GetFranchiseCharacters(id);
            if (!c.Any())
            {
                return NotFound();
            }

            return Ok(c);
        }
        
        /// <summary>
        /// Returns all movies in a Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Movies</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieWithoutFranchiseDTO>>> MoviesInFranchise(int id)
        {
            IEnumerable<MovieWithoutFranchiseDTO> m = await _franchiseService.GetFranchiseMovies(id);
            if (!m.Any())
            {
                return NotFound();
            }

            return Ok(m);
        }
    }
}
