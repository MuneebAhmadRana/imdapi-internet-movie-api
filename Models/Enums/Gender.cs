﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDapi.Models.Enums
{
    /// <summary>
    /// Enum for gender |
    /// 0 = Male |
    /// 1 = Female |
    /// 2 = Other |
    /// </summary>
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}
