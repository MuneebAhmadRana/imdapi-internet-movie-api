using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMDapi.Models.Domain
{
    /// <summary>
    /// Franchise domain model
    /// </summary>
    public class FranchiseDM
    {
        [Required]
        [Key]
        public int FranchiseId { get; set; }

        [Required]
        [MaxLength(255, ErrorMessage = "Franchise name cannot be longer than 255 characters")]
        public string Name { get; set; }

        [MaxLength(2000, ErrorMessage = "Description cannot be longer than 255 characters")]
        public string Description { get; set; }

        public ICollection<MovieDM> Movies { get; set; }
    }
}