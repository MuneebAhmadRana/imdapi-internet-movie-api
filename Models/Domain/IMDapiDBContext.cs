﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDapi.Models.Domain
{
    /// <summary>
    /// Database context
    /// </summary>
    public class IMDapiDBContext : DbContext
    {
        public IMDapiDBContext(DbContextOptions options):base(options)
        {
            
        }
        
        public DbSet<CharacterDM> Characters { get; set; }
        public DbSet<FranchiseDM> Franchises { get; set; }
        public DbSet<MovieDM> Movies { get; set; }
    }
}
