﻿using IMDapi.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMDapi.Models.Domain
{
    /// <summary>
    /// Character domain model
    /// </summary>
    public class CharacterDM
    {
        [Required]
        [Key]
        public int CharacterId { get; set; }

        [Required]
        [MaxLength(255, ErrorMessage = "Full name cannot be longer than 255 characters")]
        public string FullName { get; set; }

        [MaxLength(255, ErrorMessage = "Full name cannot be longer than 255 characters")]
        public string Alias { get; set; }

        [Required]
        public Gender Gender { get; set; }

        public string Picture { get; set; }

        public ICollection<MovieDM> Movies { get; set; }
    }
}