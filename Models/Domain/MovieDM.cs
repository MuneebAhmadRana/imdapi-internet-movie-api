using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMDapi.Models.Domain
{
    /// <summary>
    /// Movie domain model
    /// </summary>
    public class MovieDM
    {
        [Required]
        [Key]
        public int MovieId { get; set; }

        [Required]
        [MaxLength(255, ErrorMessage = "Movie name cannot be longer than 255 characters")]
        public string MovieTitle { get; set; }

        public string Genre { get; set; }

        [Required]
        [Range(0, 9999)]
        public int ReleaseYear { get; set; }

        [MaxLength(255, ErrorMessage = "Movie name cannot be longer than 255 characters")]
        public string Director { get; set; }

        [MaxLength(255, ErrorMessage = "Trailer URL cannot be longer than 255 characters")]
        public string TrailerURL { get; set; }
        [Required]
        public int FranchiseId { get; set; }
        public FranchiseDM Franchise { get; set; }
        public ICollection<CharacterDM> Characters { get; set; }
    }
}