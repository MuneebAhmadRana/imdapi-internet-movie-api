using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMDapi.Models.DTO.MovieDTOs
{
    /// <summary>
    /// Movie DTO for showing all props
    /// </summary>
    public class MovieDTO
    {
        public int MovieId { get; set; }
        [Required]
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string TrailerURL { get; set; }
        public int FranchiseId { get; set; }
        public ICollection<int> CharactersId { get; set; }
    }
}