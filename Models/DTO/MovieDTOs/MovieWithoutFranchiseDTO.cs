﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDapi.Models.DTO.MovieDTOs
{
    /// <summary>
    /// DTO for showing movie without franchises
    /// </summary>
    public class MovieWithoutFranchiseDTO
    {
        public int MovieId { get; set; }
        [Required]
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string TrailerURL { get; set; }

    }
}
