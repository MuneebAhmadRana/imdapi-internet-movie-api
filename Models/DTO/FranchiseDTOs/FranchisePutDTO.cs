using System.Collections.Generic;

namespace IMDapi.Models.DTO.FranchiseDTOs
{
    /// <summary>
    /// Franchise DTO for posting a new Franchise
    /// </summary>
    public class FranchisePostDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}