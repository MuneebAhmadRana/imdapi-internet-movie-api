using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMDapi.Models.DTO.FranchiseDTOs
{
    /// <summary>
    /// Franchise DTO for showing all props
    /// </summary>
    public class FranchiseDTO
    {
        [Key]
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<int> MoviesId { get; set; }
    }
}