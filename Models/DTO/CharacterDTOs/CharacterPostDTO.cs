﻿using IMDapi.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDapi.Models.DTO.CharacterDTOs
{
    /// <summary>
    /// Character DTO for posting a new character
    /// </summary>
    public class CharacterPostDTO
    {
        [Required]
        public string FullName { get; set; }
        public string Alias { get; set; }
        [Required]
        public Gender Gender { get; set; }
        public string Picture { get; set; }
        public ICollection<int> MoviesId { get; set; }
    }
}
