using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using IMDapi.Models.Enums;

namespace IMDapi.Models.DTO.CharacterDTOs
{
    /// <summary>
    /// Character DTO for showing all props
    /// </summary>
    public class CharacterDTO
    {
        public int CharacterId { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Alias { get; set; }
        [Required]
        public Gender Gender { get; set; }
        public string  Picture { get; set; }
        public ICollection<int> MoviesId { get; set; }
    }
}