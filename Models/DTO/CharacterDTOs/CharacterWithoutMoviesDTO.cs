﻿using IMDapi.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IMDapi.Models.DTO.CharacterDTOs
{
    /// <summary>
    /// Character DTO for showing Character without movies
    /// </summary>
    public class CharacterWithoutMoviesDTO
    {
        public int CharacterId { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Alias { get; set; }
        [Required]
        public Gender Gender { get; set; }
        public string Picture { get; set; }
    }
}
