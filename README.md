# IMDapi
### Created by Andreas Vu and Muneeb Rana
Full documentation is created with Swagger

## Features
*   Full CRUD for
    *   Characters
    *   Movies
    *   Franchises
*   Get all characters from a movie
*   Get all characters from a franchise
*   Get all movies from a franchise

## Build/Run the Application
- The user must change the SQLServer name. This can be done by changing the connectionstring `IMDapiDatabase` in the `/appsettings.json` to their local Microsoft SQLServer.
- If using terminal, 
    - Use `dotnet restore` to restore all nuget packages
    - Use `dotnet run` to run once.
    - Use `dotnet watch run` if you want to re-run on save
    - Use `donet build` to just build the project
- If using Visual Studio
    - Press the IIS Express Button to run in debugging mode.
    - Press `Ctrl + F5` to run without debug mode. 
